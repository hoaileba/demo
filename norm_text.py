import re


def remove_x(temp,text):
    # text = ' '.join(text.split(' '))
    word =text.split(' ')
    # print(word)
    
    text_norm = ''
    pre_word = ''
    for w in word:
        m = temp.match(w)
        # print(w)
        if m:
            items = m.groups()
            # print(items)
            for i in items[:3]:
                if i != 'x' and i != None:
                    text_norm += i+' '
                else :
                    text_norm += ''
        else :
            if pre_word == w and pre_word in [',']:
                text_norm+=''
            else:
                text_norm += w+' '
        if w != '':
            pre_word = w
    return text_norm
        

def norm(text):
    # text = text.lower()
    text = re.sub(" x ", " ", text)
    text = re.sub("x ", " ", text)
    # text = re.sub("(x)([0-9])", "", text)
    text = re.sub(r'[+;]', " , ", text)
    text = re.sub(r"['@#$!(/)[\]:><=`~]", " ", text)
    text = re.sub("[-]", " , ", text)
    word = text.split(' ')
    temp = re.compile("([0-9]+)([a-zA-ZÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÍÌỈĨỊÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợíìỉĩịúùủũụưứừửữựýỳỷỹỵđ]+)")
    temp1 = re.compile("(\s*x)([0-9]+)")
    temp2 = re.compile("([0-9]+)(x)([0-9]+)")
    temp3 = re.compile("([0-9]+)(x)(([A-ZÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÍÌỈĨỊÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ]+)([a-záàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợíìỉĩịúùủũụưứừửữựýỳỷỹỵđ]+))")
    text_norm = remove_x(temp1, text)
    text_norm = remove_x(temp2, text_norm)
    text_norm = remove_x(temp3, text_norm)
    # print(text_norm)
    text_norm = remove_x(temp , text_norm)
 
    text = text_norm.lower()
    text = ' '.join(text.split())
    return text

if __name__ == "__main__" : 
    text = ["1x Sách T1+T2 BĐBĐ","1 x2 ÁO A01 SIZE 36DA NÂU x 1"
    ,"3x áo phông nam" ,"3x1x TG12-Bộ thun/Màu: đỏ 3, Size:L", "3xXo xun",
    "1 x GIAY DÉP-Đồ Da",
    "1 x A10S- Tai nghe BLUETOOTH",
    "1 x 4ÁOJ đủ màu",
    "1 x Tranh 3D 9013E",
    "1 x Sũa, (1 lon) Similac (K/m: 1 lon)",
    "1 x 1xQuần Áo [52/160/ 1Quần Dai(Kem) + 1Bộ B9(DEN)",
    "1 x 1 kinh T 0.75 x2",
    "1 x ['Mua 1 compo 199k +sds']" ,
    "1 x 1 x DL21, SIZE:2XL",
    "1 x 1 x DL23 - BỘ Lụa/MÀU:XANH;;;MINKI/SIZE:M, Màu:Kem ", "m4- 20cm","1 x 1xQuần Áo [52/160/ 1Quần Dai(Kem) + 1Bộ +-B9(DEN)"]
    t = "ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬÉÈẺẼẸÊẾỀỂỄỆÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÍÌỈĨỊÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐ"
    print(t.lower())
    for t in text :
        print(t,' ####### ' ,norm(t))