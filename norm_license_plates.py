import re 

def norm_license(text):
    chars = list(text)
    norm_text = ''
    list_symbol = '!@#$%^&*()_+`~?><,."/-\|][}{'
    for char in chars :
        if char in list_symbol:
            norm_text += ', '
        else :
            norm_text += char + ' '
    print(norm_text)
if __name__ == "__main__":
    text = "51F-970.22"
    norm_license(text) 
